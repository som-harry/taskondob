﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaskOnDob.Helpers;

namespace TaskOnDob.Services
{
    public static  class infoServices
    {
      public static string GetName()
        {
            Console.WriteLine("Enter your name with your surname first");

            string name = Console.ReadLine();

            Console.WriteLine("......");

            return name;
        }

        public static string GetAge()
        {
            Console.WriteLine("Enter your date of birth");

            string date = Console.ReadLine();
            string age = "";

            try
            {
                var currentDate = DateTimeOffset.Parse(date);

                age = currentDate.CalculateAge();
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return age;
        }
    }
}
