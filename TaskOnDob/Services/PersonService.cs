﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaskOnDob.Models;

namespace TaskOnDob.Services
{
    public class PersonService
    {
        private static Person person;

        public PersonService(string fname, string LastName, string age)
        {
            person = new Person()
            {
                FirstName = fname,
                LastName = LastName,
                Age = age
            };
        }
        public static void GetPersonalInfo()
        {
            Console.WriteLine($"My fullname is {person.LastName} {person.FirstName}  and i am {person.Age} years old");
        }
    }
}
